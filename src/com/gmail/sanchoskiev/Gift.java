package com.gmail.sanchoskiev;

import java.util.*;

import com.gmail.sanchoskiev.Sweets.*;

public class Gift {
    int totalPrice;
    int numOfSweets;
    int giftWeight;
    SweetController generator = new SweetController();
    List<Sweet> sweetList = new ArrayList<Sweet>();

    public Gift() {
        addSweet(450);
    }

    void addSweet(int weight) {
        this.giftWeight = 0;
        this.numOfSweets = 0;
        while (this.giftWeight < weight) {
            Sweet newSweet = generator.generateSweet();
            sweetList.add(newSweet);
            this.giftWeight += newSweet.getWeight();
            this.numOfSweets++;
        }

    }

    public void printGiftContain() {
        System.out.println("Your giff contains ");
        for (Sweet sweet : sweetList) {
            System.out.print(" " + sweet.getClass().getSimpleName());
        }
        System.out.println();
    }

    @Override
    public String toString() {
        return "Gift weight = " + giftWeight + " contains " + numOfSweets + " sweets and has price " + totalPrice;
    }

    public void printSweetsByWeightRange(double weightFrom, double weightTo) {
        System.out.println("The sweets in the Your weight range from " + weightFrom + " to " + weightTo + ":");
        for (int i = 0; i < sweetList.size(); i++) {
            if ((weightFrom < sweetList.get(i).getWeight()) && (sweetList.get(i).getWeight() < weightTo)) {
                System.out.println(sweetList.get(i));
            }
        }
    }

    public Sweet findSweetByShugarRange(int shugarFrom, int sugarTo) {
        Sweet foundedSweet = null;
        for (int i = 0; i < sweetList.size(); i++) {
            if (shugarFrom < sweetList.get(i).getShugar() && sweetList.get(i).getShugar() < sugarTo) {
                foundedSweet = sweetList.get(i);
            }
        }
        return foundedSweet;
    }


}


