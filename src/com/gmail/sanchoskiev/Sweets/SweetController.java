package com.gmail.sanchoskiev.Sweets;

import java.util.Random;

/**
 * Created by alex on 06.11.16.
 */
public class SweetController {
    public Sweet generateSweet(){
        int randomNumber;
        Random random = new Random();
        randomNumber = random.nextInt(3);
        switch (randomNumber) {
            case 1:
                return new JellySweet();
            case 2:
                return new ChokolateSweet();
            default:
                return new Cookie();
        }


    }

}
