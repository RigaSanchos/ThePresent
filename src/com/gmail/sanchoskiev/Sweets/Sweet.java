package com.gmail.sanchoskiev.Sweets;
import java.util.Comparator;


public abstract class Sweet implements Comparator<Sweet> {
    int shugar, weight;
    String name;

    void setName(String name){
        this.name = name;
    }

    String getName(){
        return name;
    }

    void setWeight(int weight){
        this.weight = weight;
    }

    public int getWeight(){
        return weight;
    }

    public void setShugar(int shugar){
        this.shugar = shugar;
    }

    public int getShugar(){return shugar;}

    public Sweet(int shugar, int weight, String name){
        this.shugar = shugar;
        this.weight = weight;
        this.name = name;
        if (name == null) System.out.println("Wrong name");
        if (shugar <=0) System.out.println("Wrong shugar");
        if (weight <=0) System.out.println("Wrong weight");
    }

    @Override
    public String toString(){
        return "Shugar = " + shugar + " Name of sweet " + getClass().getSimpleName() + " Weight is" + weight;
    }

    @Override
    public int hashCode(){
        return (int) (61*shugar + 31*weight + name.hashCode());
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;

        Sweet sweet = (Sweet) obj;
        if (sweet.shugar != shugar){return false;}
        if (!sweet.name.equals(name)){return false;}
        if (weight != sweet.weight) {return false;}
        return true;
    }

    public int compareTo(Sweet sweet) {
        return (this.name).compareTo(sweet.name);
    }

    public class ByWeightComparator implements Comparator<Sweet> {
        public int compare (Sweet s1, Sweet s2){
            if (s1.getWeight()<s2.getWeight()) return -1;
            if (s1.getWeight()>s2.getWeight()) return 1;
            else return 0;
        }
    }
}
